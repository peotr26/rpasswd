BINARY_NAME=rpasswd
 
all: build test

build:
	mkdir build
	go build -o build/${BINARY_NAME} src/main.go

install: build
	cp build/rpasswd ~/.local/bin

test:
	go test -v src/main.go

run: build
	./build/${BINARY_NAME}

clean:
	go clean
	rm build/${BINARY_NAME}
	rmdir build
