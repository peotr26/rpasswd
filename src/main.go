package main

import (
	"fmt";
	"os";
	"time";
	"math/rand";
	"strconv"
)

// Array containing the 4 sets of characters use to compose the password.
var data = [4][]string{
	{ // Complete set of letters, digits and symbols.
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
		"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
		"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"!", "#", "$", "%", "&", "*", "+", "?", "@", "^",

	},
	{ // Only letters.
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
		"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
		"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	},
	{ // Only digits.
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	},
	{ // Letters and digits.
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
		"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
		"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	},
}

// Stores the settings used for the execution.
// The first setting is the size of the password returned and
// the second one is the set that is use to choose the set of characters composing the password.
// By default, the length is set at 16 and the set to complete.
var mode = [2]int{16, 0}

// Parses the arguments given to the program, and applies them to the current execution of the program.
func args_parsing() {
	tmp := os.Args[1:]
	for _, e := range tmp {
		eInt, isInt := strconv.Atoi(e)
		if isInt == nil { // Sets the size of the password.
			mode[0] = eInt
		} else if string(e) == "-c" {  // Sets to complete.
			mode[1] = 0
		} else if string(e) == "-a" {  // Sets to letters only.
			mode[1] = 1
		} else if string(e) == "-n" {  // Sets to digits only.
			mode[1] = 2
		} else if string(e) == "-an" { // Sets to letters and digits.
			mode[1] = 3
		}
		
	}
}

// Chooses a random element in an array.
func rand_choose(list []string) string {
	return list[rand.Intn(len(list))]
}

func main() {
	args_parsing()
	var password string
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < mode[0]; i++ {
		password += rand_choose(data[mode[1]])
	}
	fmt.Println(password)
}
