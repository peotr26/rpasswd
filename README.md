# rpasswd

This program will generate a random password using three levels of complexity using the random number generator from your computer. You can choose the length of the password.

## Instructions

The program can take 2 arguments after the file name. The first one correspond to the length of the password (the default length is 16 characters), and the second one to the level of complexity.  

```bash
rpasswd

# With arguments

rpasswd -an 12
```

### Installation

Make sure you have `go` on your system.

Here are the instruction to build and install:

```bash
git clone https://codeberg.org/peotr26/rpasswd.git
cd rpasswd

# To build
make

# To install
make install
```

## Different level of complexity:

- Complete (`-c`) (Default): the password that will be generated will contain ASCII letters, digits, and the following symbols :
  - `!`
  - `#`
  - `$`
  - `%`
  - `&`
  - `*`
  - `+`
  - `?`
  - `@`
  - `^`
- Alphabetical (`-a`): the password that will be generated will only contain ASCII letters, both capitalize and minimize.
- Numerical (`-n`): the password that will be generated will only contain digits.
- Alphanumerical (`-an`): the password that will be generated will only contain ASCII letters and digits.
